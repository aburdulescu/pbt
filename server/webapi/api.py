import sqlite3
import os
from bottle import route, run, template, request, static_file


DBPATH = '/pbt/torrents/'

@route('/')
def home():
    d = os.listdir(DBPATH)
    res = {
        'data': d
    }
    return res

@route('/<filepath:path>')
def path(filepath):
    if str(filepath).endswith('.torrent'):
        return static_file(filepath, root=DBPATH)
    else:
        f = os.listdir(DBPATH+filepath)
        return {'data': f}

def main():
    run(host='0.0.0.0', port=8080, debug=True)
