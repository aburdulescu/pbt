from setuptools import setup

setup(name='pbt-webapi',
      version='0.1',
      description='pbt-webapi',
      py_modules=['api'],
      install_requires=[
          'bottle'
      ],
      entry_points={
          'console_scripts': ['pbt-webapi=api:main']
      },
)
