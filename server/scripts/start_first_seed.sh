#!/bin/bash

DOWNLOADDIR=/pbt/files/
WATCHDIR=/pbt/torrents/
LOGFILE=/pbt/transmissio-daemon.log

OPTIONS="--no-dht \
         --no-lpd \
         --watch-dir $WATCHDIR \
         --download-dir $DOWNLOADDIR \
         --logfile $LOGFILE"

transmission-daemon $OPTIONS
