#!/bin/bash

TORRENTSDIR=torrents
FILESDIR=files

mkdir -p $FILESDIR $TORRENTSDIR

END=3
for d in $(seq $END)
do
    for i in $(seq $END)
    do
        filename="dir"$d"_file"$i
        echo "This is "$filename".txt" > $filename.txt
        transmission-create $filename.txt -t http://172.18.0.3:6969/announce -o $filename.torrent -p
    done
done

mv *.txt $FILESDIR
mv *.torrent $TORRENTSDIR
