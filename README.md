# PBt
PBt(Private BitTorrent) is a system used for creating a private 
BitTorrent swarm.

## Design

The system consists of a server(pbt server) and multiple
clients(pbt client).

### PBt server components

#### A BitTorrent tracker

This component tracks all files in the swarm and also introduces pbt clients
among each other.

Opentracker is used. The latest version can be downloaded from [here](https://erdgeist.org/gitweb/opentracker/).
Opentracker depends on the libowfat library. This library should be availabele
in the distro package repository, otherwise it can be retrived from [here](http://www.fefe.de/libowfat/).

#### A web server

A web server that displays all files available in the BitTorrent swarm and is
needed so that the pbt clients can start downloading the files.

Python with flask is used.

### A BitTorrent client

Used for the initial uploading of the files(first peer is always the pbt server,
after more peers have the file pbt server will stop uploading the files).
See below more details.

### PBt client components

#### A BitTorrent client

This component is used by the pbt server and the clients and is responsible for
downloading/uploading the files from/to the swarm.

Transmission is used. More precisely transmission daemon.
Transmission is available in almost all distros, if not it can be retrieved from
[here](https://github.com/transmission/transmission).

#### A command line interface

The cli will query the pbt server and will display the available files.
The user then will instruct the cli to download one of the files.

Python is used(or use go?). For this cli the package
[transmission-fluid](https://github.com/edavis/transmission-fluid) is used for
sending requests to transmission daemon.

The cli is not mandatory. Transmission web interface can be used to interact
with the daemon, but having a cli will be easier for users to interact with 
the pbt server and the deamon and will also give the possibility for automation.
