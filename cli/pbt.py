import requests
import sys
import subprocess
import os
import time
import yaml
import click
import http.client # for HTTP response codes
import transmission
import base64
import json


# New cli:
# |------------+--------------+------------------------------------|
# | Subcommand | Arg          | Opt                                |
# |------------+--------------+------------------------------------|
# | list       | n            | -l latest(sort and pick the first) |
# |            |              | -s sort output                     |
# |            |              | -r reverse sort                    |
# |------------+--------------+------------------------------------|
# | download   | remotefile   | n                                  |
# |------------+--------------+------------------------------------|
# | status     | file/id/none | n                                  |
# |------------+--------------+------------------------------------|
# | start      | file/id      | n                                  |
# |------------+--------------+------------------------------------|
# | stop       | file/id      | n                                  |
# |------------+--------------+------------------------------------|
# | remove     | file/id      | n                                  |
# |------------+--------------+------------------------------------|


@click.group()
def main():
    pass

@main.group()
def daemon():
    pass

@main.group()
def torrent():
    pass

@daemon.command()
def start():
    pass

@daemon.command()
def stop():
    pass

@torrent.command()
@click.option('--latest', '-l', is_flag=True,
              help='list latest(i.e. reverse sort the results and pick the first file)')
@click.option('--sort', '-s', is_flag=True,
              help='print sorted output')
@click.option('--reverse-sort', '-r', is_flag=True,
              help='print sorted output in reverse order')
@click.option('--token', '-t', type=str, default='',
              help='print all files that contain the token')
def list(latest, sort, reverse_sort, token):

    if latest and sort or latest and reverse_sort or sort and reverse_sort:
        print('error: the options you selected are mutally exclusive.')
        sys.exit(1)

    url = build_url()

    try:
        res = requests.get(url, timeout=3)
        if res.status_code == 200:
            res_data = res.json()['data']

            if token == '':
                data =res_data
            else:
                data = [d for d in res_data if token in d]

            if sort:
                data.sort()
                for d in data:
                    print(d)
            elif reverse_sort:
                data.sort(reverse=True)
                for d in data:
                    print(d)
            elif latest:
                data.sort(reverse=True)
                print(data[0])
            else:
                for d in data:
                    print(d)

        else:
            log_http_error(res.status_code)
    except requests.exceptions.ConnectionError:
        print('Error: could not connect to', url)

@torrent.command()
@click.argument('remotefile', type=str)
def download(remotefile):
    if not remotefile.endswith('.torrent'):
        print('"%s" does not end with "*.torrent"' % remotefile)
        sys.exit(1)

    # get torrent file
    url = build_url(filename=remotefile)
    metainfo = download_metainfo(url)

    start_tr_daemon()

    # start transmission-fluid session
    tr = transmission.Transmission()

    # add torrent
    metainfo_b64 = base64.b64encode(metainfo).decode('utf-8')
    r = tr('torrent-add', metainfo=metainfo_b64)

    if 'torrent-added' in r:
        torrent_info = r['torrent-added']
        print('Torrent added:')
        print(json.dumps(torrent_info, sort_keys=True, indent=4))
    else:
        print('Error when trying to add torrent:')
        print(json.dumps(r, sort_keys=True, indent=4))
        sys.exit(1)

    while True:
        r = tr('torrent-get',
               ids=[torrent_info['hashString']],
               fields=['percentDone'])

        # percentDone in [0,1]
        percentDone = int(float(r['torrents'][0]['percentDone']) * 100)

        print('\r{0} [{1}] {2}%'.format(torrent_info['name'], '#' * percentDone, percentDone),
              end='', flush=True)

        if percentDone == 100:
            print('')
            break

        time.sleep(1)

@torrent.command()
@click.argument('fileid', type=int, default=0)
def status(fileid):
    if fileid == 0:
        if runcmd('transmission-remote -l', output=True) == False:
            return
    else:
        if runcmd('transmission-remote -t '+str(fileid)+' -l', output=True) == False:
            return

@torrent.command()
@click.argument('fileid', type=str)
def start(fileid):
    pass

@torrent.command()
@click.argument('fileid', type=str)
def stop(fileid):
    pass

@torrent.command()
@click.argument('fileid', type=str)
def remove(fileid):
    pass


def log_http_error(status_code):
    print('HTTP error:', http.client.responses[status_code])

def build_url(filename=''):
    config = read_config()
    if config == None:
        sys.exit(1)

    url = 'http://' + config['webapi-addr'] + ':8080'

    if filename != '':
        url += '/' + filename

    return url

def download_metainfo(url):
    try:
        res = requests.get(url, timeout=5)
        if res.status_code == 200:
            return res.content
        else:
            log_http_error(res.status_code)
            sys.exit(1)
    except requests.exceptions.ConnectionError:
        print('Error: could not connect to', url)
        sys.exit(1)


def runcmd(cmd, check=True, output=False, delay=0):
    # TODO: still prints output
    if check == True:
        try:
            if output == True:
                res = subprocess.run(cmd,
                                     shell=True, check=True)
            else:
                res = subprocess.run(cmd,
                                     shell=True, check=True,
                                     stdout=subprocess.DEVNULL,
                                     stderr=subprocess.DEVNULL)
            if delay != 0:
                time.sleep(2)
            return True
        except subprocess.CalledProcessError:
            # TODO: move into a log file
            print('error:"'+cmd+'".')
            return False
    else:
        if output == True:
            res = subprocess.run(cmd, shell=True)
        else:
            res = subprocess.run(cmd, shell=True,
                                 stdout=subprocess.DEVNULL,
                                 stderr=subprocess.DEVNULL)
        if delay != 0:
            time.sleep(2)
        return True

def start_tr_daemon():
    if runcmd('pidof transmission-daemon') == False:
        print('starting daemon ... ', end='')
        cmd = '''
        transmission-daemon \
        --no-dht \
        --no-lpd \
        --download-dir /pbt/files/ \
        --logfile /pbt/transmissio-daemon.log
        '''
        if runcmd(cmd, delay=2) == False:
            sys.exit(1)
        # TODO: there still is a delay after this is printed
        print('done')

def read_config():
    config = None
    with open("/etc/pbt/config.yml", 'r') as f:
        try:
            config = yaml.load(f)
        except yaml.YAMLError as exc:
            print(exc)

    return config


if __name__ == '__main__':
    main()
