from setuptools import setup

setup(name='pbt',
      version='0.1',
      description='pbt',
      url='http://gitlab.com/aburdulescu/pbt',
      author='aburdulescu',
      author_email='andrei.burdulescu@protonmail.com',
      py_modules=['pbt'],
      install_requires=[
          'requests',
          'pyyaml',
          'click',
          'transmission-fluid'
      ],
      entry_points={
          'console_scripts': ['pbt=pbt:main']
      },
      data_files=[
          ('/etc/pbt', ['config.yml']),
      ]
)
